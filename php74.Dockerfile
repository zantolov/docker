###### BASE STAGE START #######
FROM php:7.4-fpm-alpine as base
ARG USER_ID=1000
ARG GROUP_ID=1000
# Create app user and group with UID same as provided argument to avoid permission issues and conflicts
RUN echo "app:app user and group with IDs $USER_ID:$GROUP_ID" \
    && addgroup -g $GROUP_ID -S app \
    && adduser -S app -D -u $USER_ID -G app
# Add runtime dependencies to the env variable
ENV RUNTIME_DEPS="icu-dev bash rabbitmq-c rabbitmq-c-dev libzip-dev postgresql-dev"
RUN apk add --update --no-cache $RUNTIME_DEPS $PHPIZE_DEPS
RUN docker-php-ext-install pdo pdo_mysql pdo_pgsql zip intl
RUN pecl install amqp-1.9.4 \
    && docker-php-ext-enable amqp
RUN pecl install redis \ 
    && docker-php-ext-enable redis

RUN apk del $PHPIZE_DEPS


###### BUILD-TOOLS STAGE START #######
FROM base as build-tools
# Install composer
ENV COMPOSER_DEPS zip unzip wget
ADD install-composer.sh .
RUN apk add --update --no-cache git $COMPOSER_DEPS \
        && chmod u+x install-composer.sh \
        && ./install-composer.sh \
        && composer --version \
        && apk del --purge $COMPOSER_DEPS
RUN mkdir -p /opt/app && chown app /opt/app
USER app
# Install https://github.com/hirak/prestissimo for faster composer installs
RUN composer global require hirak/prestissimo maglnet/composer-require-checker


###### DEV-TOOLS STAGE START #######
FROM build-tools as dev-tools
USER root
# Install XDebug
RUN apk add --no-cache $PHPIZE_DEPS \
    && pecl install -f xdebug \
    && docker-php-ext-enable xdebug \
    && apk del --purge $PHPIZE_DEPS
ADD xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini


###### STANDALONE-TOOLS STAGE START #######
FROM base as standalone-tools
RUN wget https://get.symfony.com/cli/installer -O - | bash \
    && export PATH="$HOME/.symfony/bin:$PATH" \
    && mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
    && symfony --help
