# Nginx

```
FROM registry.gitlab.com/zantolov/docker:nginx as nginx
COPY --from=prod --chown=app:app  /opt/app/public /opt/app/public
USER app
```